================================================================================
Inline Errors Module
================================================================================

================================================================================
NOTE:	Requires the jQuery ScrollTo plugin which can be downloaded from
      http://flesler.blogspot.com/2007/10/jqueryscrollto.html
      
      Demo: http://demos.flesler.com/jquery/scrollTo
================================================================================

The Inline Errors module allows you to move form messages from the top of the
page to just above where your form appears.

An example usage is with the comment form, which is typically placed at the
bottom of the page. Should an error be generated when someone submits,
they jump back to the top of the page. So to get back to the comment form,
you have to scroll back to the bottom of the page.

With a little jQuery and the jQuery ScrollTo plugin, Inline Errors will
capture the $messages, place them just above your form and move the page
to the top of the form.

You can enable Inline Errors for any form that implements hook_form().

--------------------------------------------------------------------------------
For more information on the project and to submit issues and patches
visit the following page: http://drupal.org/project/inline_errors


================================================================================
Known Issues
================================================================================
Inline Errors does not work with the core Poll module

================================================================================
Credits
================================================================================
Inline Errors contributed by jsfwd / ninjagirl (http://ninjagirl.com)