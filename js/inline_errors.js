/**
 * If there are form error on a page that has 'Inline Errors' enabled...
 * move drupal messages above the form
 */

if (Drupal.jsEnabled) {
  $(document).ready(function(){
    $form_id = $('#' + Drupal.settings.form_id);
    var error_msg = $('.messages.error');
    var form_errors = error_msg.html();
    
    // drupal appends #form-path to the action of the form
    // we want to remove this so that jquery can just handle the page moving
    var form_action_string = $form_id.attr("action").split('#');
    $form_id.attr("action", form_action_string[0]);
    
    if (form_errors != null) {
      // add the message error html to a new div above the form
      // hide the message status
      // move the page to the form
      $form_id.prepend('<div id="msg-errors" class="messages error">' + form_errors + '</div>');
      error_msg.hide();
      $.scrollTo($form_id);
    }
  });
}